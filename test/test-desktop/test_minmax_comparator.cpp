//
// Created by vincent on 20/01/2021.
//

#include <unity.h>
#include <MinMaxComparator.h>

void test_init_temp(void) {
    MinMaxComparator mmComp(15.2, 0);
    TEST_ASSERT_EQUAL_FLOAT(15.2, mmComp.GetTempMin());
    TEST_ASSERT_EQUAL_FLOAT(15.2, mmComp.GetTempMax());
}

void test_init_rh(void) {
    MinMaxComparator mmComp(0, 47.6);
    TEST_ASSERT_EQUAL_FLOAT(47.6, mmComp.GetRHMin());
    TEST_ASSERT_EQUAL_FLOAT(47.6, mmComp.GetRHMax());
}

void test_set_temp_min(void) {
    MinMaxComparator mmComp(15.2, 47.6);
    mmComp.SetTemp(10);
    TEST_ASSERT_EQUAL_FLOAT(10,mmComp.GetTempMin());
    TEST_ASSERT_EQUAL_FLOAT(15.2, mmComp.GetTempMax());
}

void test_set_temp_max(void) {
    MinMaxComparator mmComp(15.2, 47.6);
    mmComp.SetTemp(24.6);
    TEST_ASSERT_EQUAL_FLOAT(15.2, mmComp.GetTempMin());
    TEST_ASSERT_EQUAL_FLOAT(24.6, mmComp.GetTempMax());
}

void test_set_rh_min(void) {
    MinMaxComparator mmComp(15.2, 47.6);
    mmComp.SetRH(22.3);
    TEST_ASSERT_EQUAL_FLOAT(22.3, mmComp.GetRHMin());
    TEST_ASSERT_EQUAL_FLOAT(47.6, mmComp.GetRHMax());
}

void test_set_rh_max(void) {
    MinMaxComparator mmComp(15.2, 47.6);
    mmComp.SetRH(98.2);
    TEST_ASSERT_EQUAL_FLOAT(47.6, mmComp.GetRHMin());
    TEST_ASSERT_EQUAL_FLOAT(98.2, mmComp.GetRHMax());
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(test_init_rh);
    RUN_TEST(test_init_temp);
    RUN_TEST(test_set_temp_min);
    RUN_TEST(test_set_temp_max);
    RUN_TEST(test_set_rh_min);
    RUN_TEST(test_set_rh_max);
    UNITY_END();

    return 0;
}