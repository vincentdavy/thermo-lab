//
// Created by vincent on 13/02/2021.
//

#include <math.h>
#include "MinMaxComparator.h"

MinMaxComparator::MinMaxComparator(const float &temp, const float &rh) {
    if (!isnan(temp)) {
        tempMin = temp;
        tempMax = temp;
    }
    if (!isnan(rh)) {
        rhMin = rh;
        rhMax = rh;
    }
}

void MinMaxComparator::SetTemp(const float &temp) {
    SetMinMax(tempMin, tempMax, temp);
}

void MinMaxComparator::SetRH(const float &rh) {
    SetMinMax(rhMin, rhMax, rh);
}

void MinMaxComparator::SetMinMax(float &minDest, float &maxDest, float const &value) {
    if (!isnan(value)) {
        if (value < minDest) {
            minDest = value;
        } else if (value > maxDest) {
            maxDest = value;
        }
    }
}

float MinMaxComparator::GetTempMin() const {
    return tempMin;
}

float MinMaxComparator::GetTempMax() const {
    return tempMax;
}

float MinMaxComparator::GetRHMin() const {
    return rhMin;
}

float MinMaxComparator::GetRHMax() const {
    return rhMax;
}
