//
// Created by vincent on 13/02/2021.
//

#ifndef THERMO_LAB_MINMAXCOMPARATOR_H
#define THERMO_LAB_MINMAXCOMPARATOR_H

/*!
 * @brief Class to manage the comparator of min / max for temp and RH
 */
class MinMaxComparator {
private:
    float tempMin = 0;
    float tempMax = 0;
    float rhMin = 0;
    float rhMax = 0;

    /*!
     * Set the current value into right var, if match the min or the max
     * @param minDest the min destination var
     * @param maxDest the max destination var
     * @param value the value to make comparison
     */
    static void SetMinMax(float &minDest, float &maxDest, float const &value);

public:
    /*!
     * @brief Create the comparator with intialized values
     * @param temp the init temp value
     * @param rh the init rh value
     */
    MinMaxComparator(float const &temp, float const &rh);

    /*!
     * @brief get the min temp
     * @return the min temp
     */
    float GetTempMin() const;

    /*!
     * @brief get the min temp
     * @return the max temp
     */
    float GetTempMax() const;

    /*!
     * @brief get the min RH
     * @return the min RH
     */
    float GetRHMin() const;

    /*!
     * @brief get the max RH
     * @return the max rh
     */
    float GetRHMax() const;

    /*!
     * @brief set the current temp and compute the min and max
     * @param temp the temperature to set
     */
    void SetTemp(float const &temp);

    /*!
     * @brief set the current RH and compute the min and max
     * @param rh the RH to set
     */
    void SetRH(float const &rh);
};

#endif //THERMO_LAB_MINMAXCOMPARATOR_H
