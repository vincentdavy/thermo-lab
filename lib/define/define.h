//
// Created by vincent on 22/01/2021.
//

#ifndef THERMO_LAB_DEFINE_H
#define THERMO_LAB_DEFINE_H

#include <stdint-gcc.h>

// button part
#define INT_PIN 2

// DHT part
#define DHT_PIN A3

// LCD part
#define LCD_I2C_ADDR    0x27
#define LCD_ROWS        2
#define LCD_COLS        16

// Timer part
#define TIMER_OVERFLOW_COUNT 10

#endif //THERMO_LAB_DEFINE_H
