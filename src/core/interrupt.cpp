
#include <ledmanager/LEDManager.h>
#include <Arduino.h>
#include "core.h"
#include <define.h>
#include <sleep/sleep.h>

void Core::EnableButtonInterrupt() {
    EIMSK |= (1 << INT0);
}

void Core::DisableButtonInterrupt() {
    EIMSK &= ~(1 << INT0);
}

void Core::ButtonISR() {
    buttonInterrupt = true;
}

void Core::CheckInterrupt() {
    if (buttonInterrupt) {
        DisableButtonInterrupt();
        buttonInterrupt = false;

        Core::StartTimer();
        while (timerInterruptCount < TIMER_OVERFLOW_COUNT) {
            Sleep::ForeverSleep();
        }
        Core::StopTimer();

        if (!digitalRead(INT_PIN)) {
            lcdManager->DisplayMinMaxValues();
            Sleep::NormalSleep();
            lcdManager->DisplayInitTextAndTempRHValues();
        }

        EnableButtonInterrupt();
    }
}
