//
// Created by vincent on 07/03/2021.
//

#include "core.h"

void Core::TimerISR() {
    timerInterruptCount++;
}

ISR(TIMER2_OVF_vect) {
    Core::TimerISR();
}

void Core::StartTimer() {
    timerInterruptCount = 0;
    TCNT2 = 0; // reset timer value
    TIMSK2 |= (1 << TOIE2); // enable interrupt
    TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20); // enable prescaler to 1024
}

void Core::StopTimer() {
    TIMSK2 &= ~(1 << TOIE2); // disable interrupt
    TCCR2B &= ~(1 << CS22) & ~(1 << CS21) & ~(1 << CS20); // disable prescaler
}