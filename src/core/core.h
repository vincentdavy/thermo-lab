//
// Created by vincent on 14/02/2021.
//

#ifndef THERMO_LAB_CORE_H
#define THERMO_LAB_CORE_H

#include <lcdmanager/LCDManager.h>
#include <dht22manager/DHT22Manager.h>
#include <MinMaxComparator.h>

class Core {
private:
    LCDManager *lcdManager = nullptr;
    DHT22Manager *dht22Manager = nullptr;
    MinMaxComparator *minMaxComp = nullptr;

    static volatile bool buttonInterrupt;
    static volatile byte timerInterruptCount;

    /*!
     * @brief Init the interrupt through external pin
     */
    static void InitInterrupt();

    /*!
     * @brief Init the timer for interrupt
     */
    static void InitTimer();

    /*!
     * @brief Enable the button interrupt
     */
    static void EnableButtonInterrupt();

    /*!
     * @brief Disable the button interrupt
     */
    static void DisableButtonInterrupt();

    /*!
     * @brief Start the timer and the associated interrupt
     */
    static void StartTimer();

    /*!
     * @brief Stop the timer and disable the associated interrupt
     */
    static void StopTimer();

public:

    /*!
     * @brief Create a new core
     */
    Core();

    /*!
     * @brief First init to call
     */
    void Init();

    /*!
     * @brief Display the current temp and RH on LCD
     */
    void DisplayCurrentTempRH() const;

    /*!
     * @brief Check if a button interrupt was triggered
     */
    void CheckInterrupt();

    /*!
     * @brief Button ISR called when interrupt is triggerd
     */
    static void ButtonISR();

    /*!
     * @brief Timer ISR called after timer hit the compared number
     */
    static void TimerISR();
};

#endif //THERMO_LAB_CORE_H
