#include <ledmanager/LEDManager.h>
#include "core.h"

void Core::Init() {
    lcdManager->DisplayInitText();
    LEDManager::SetLEDStatus(true);

    float temperature = 0, humidity = 0;
    dht22Manager->ReadValues(temperature, humidity);
    lcdManager->InitPreviousData(temperature, humidity);
    minMaxComp = new MinMaxComparator(temperature, humidity);

    lcdManager->DisplayData(temperature, humidity);
    LEDManager::SetLEDStatus(false);

    InitInterrupt();
    InitTimer();
}

void Core::InitInterrupt() {
    pinMode(INT_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(INT_PIN), Core::ButtonISR, FALLING);
    sei();
}

void Core::InitTimer() {
    // Timer 2 in CTC mode with top using OCR2A
    TCCR2A &= 0x00;
    TCCR2B &= 0x00;
}