//
// Created by vincent on 14/02/2021.
//

#include <ledmanager/LEDManager.h>
#include "core.h"

volatile bool Core::buttonInterrupt = false;
volatile byte Core::timerInterruptCount = 0;

Core::Core() {
    LEDManager::Init();
    LEDManager::SetLEDStatus(true);

    lcdManager = new LCDManager(&minMaxComp);
    lcdManager->DisplayBootText();

    dht22Manager = new DHT22Manager();

    LEDManager::SetLEDStatus(false);
}

void Core::DisplayCurrentTempRH() const {
    float temperature = 0, humidity = 0;

    dht22Manager->ReadValues(temperature, humidity);
    minMaxComp->SetTemp(temperature);
    minMaxComp->SetRH(humidity);

    lcdManager->DisplayData(temperature, humidity);
}

