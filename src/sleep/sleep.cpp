//
// Created by vincent on 14/02/2021.
//

#include "sleep.h"
#include <LowPower.h>

void Sleep::InitSleep() {
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_ON);
}

void Sleep::NormalSleep() {
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_ON);
}

void Sleep::ForeverSleep() {
    LowPower.powerSave(SLEEP_FOREVER, ADC_OFF, BOD_ON, TIMER2_ON);
}
