//
// Created by vincent on 14/02/2021.
//

#ifndef THERMO_LAB_SLEEP_H
#define THERMO_LAB_SLEEP_H

/*!
 * @brief class to manage the sleep part
 */
class Sleep {
public:
    /*!
     * @brief Trigger an core sleep for 1 second
     */
    static void InitSleep();

    /*!
     * @brief Trigger an normal sleep for 4 seconds
     */
    static void NormalSleep();

    /*!
     * @brief Trigger an normal sleep without duration
     */
    static void ForeverSleep();
};


#endif //THERMO_LAB_SLEEP_H
