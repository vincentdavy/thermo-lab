//
// Created by vincent on 24/01/2021.
//

#ifndef THERMO_LAB_LCDMANAGER_H
#define THERMO_LAB_LCDMANAGER_H

#include <LiquidCrystal_I2C.h>
#include <define.h>
#include <Arduino.h>
#include <MinMaxComparator.h>
#include "LCDManager-const.h"

/*!
 * @brief Class to abstract LCD management
 */
class LCDManager {
private:
    LiquidCrystal_I2C lcd = LiquidCrystal_I2C(LCD_I2C_ADDR, LCD_COLS, LCD_ROWS);
    float previousTemperature = 0;
    float previousHumidity = 0;
    MinMaxComparator **minMaxComp = nullptr;

    /*!
     * @brief Display temp or RH value if update needed
     * @param value the value to display
     * @param previousValue  the previous value to compare if update display needed
     * @param colIndex the vol index to display
     * @param rowIndex the row index to display
     * @param forceDisplay force display (bypass the update needed check)
     */
    void ProperDisplay(const float &value, float &previousValue, uint8_t colIndex, uint8_t rowIndex,
                       bool forceDisplay = false);

    /*!
     * @brief Display a star
     * @param display true to display it, false to hide
     * @param rowIndex the row index to display
     */
    void DisplayStar(bool display, uint8_t rowIndex);

    /*!
     * @brief Display the value at specified position
     * @param value value to display
     * @param rowId the row id
     * @param colId the col id
     */
    void DisplayMinMaxValue(float value, int colId, int rowId);

public:
    /*!
     * @brief Create and initialize the LCD
     */
    explicit LCDManager(MinMaxComparator **minMaxComp);

    /*!
     * @brief Display boot start text
     */
    void DisplayBootText();

    /*!
     * @brief Display the no data default init text
     */
    void DisplayInitText();

    /*!
     * @brief Display current data on LCD
     * @param temperature the temperature as float
     * @param humidity the humidity as float
     */
    void DisplayData(float const &temperature, float const &humidity);

    /*!
     * @brief Init the previous data to have truly comparison at first displauy
     * @param temperature the temperature as float
     * @param humidity the humidity as float
     */
    void InitPreviousData(float const &temperature, float const &humidity);

    /*!
     * @brief Display the min/max RH and temperature values
     */
    void DisplayMinMaxValues();

    /*!
     * @brief Display the init text and the temp and RH values
     */
    void DisplayInitTextAndTempRHValues();
};

#endif //THERMO_LAB_LCDMANAGER_H
