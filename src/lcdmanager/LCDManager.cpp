//
// Created by vincent on 24/01/2021.
//

#include "LCDManager.h"

LCDManager::LCDManager(MinMaxComparator **minMaxComp) {
    this->minMaxComp = minMaxComp;
    lcd.init();
    lcd.createChar(DEGREE_CHAR_ID, c_degreeSign);
    lcd.clear();
    lcd.backlight();
}

void LCDManager::DisplayBootText() {
    lcd.home();
    lcd.print(F(BOOT_TEXT));
}

void LCDManager::DisplayInitText() {
    lcd.home();
    lcd.print(F(TEMP_DEFAULT_TEXT));
    lcd.write(DEGREE_CHAR_ID);
    lcd.print(F(DEGREE_SIGN_TEXT));

    lcd.setCursor(RH_START_COL_INDEX, RH_LINE_INDEX);
    lcd.print(F(RH_DEFAULT_TEXT));
}

void LCDManager::DisplayData(float const &temperature, float const &humidity) {
    ProperDisplay(temperature, previousTemperature, TEMP_COL_INDEX, TEMP_LINE_INDEX);
    ProperDisplay(humidity, previousHumidity, RH_COL_INDEX, RH_LINE_INDEX);
}

void LCDManager::ProperDisplay(const float &value, float &previousValue, uint8_t colIndex, uint8_t rowIndex,
                               bool forceDisplay) {
    if (forceDisplay || value != previousValue) {
        if (!isnan(value)) {
            lcd.setCursor(colIndex, rowIndex);
            lcd.print(value, DIGIT_NUMBER);
            if (isnan(previousValue)) {
                DisplayStar(false, rowIndex);
            }
        } else if (!isnan(previousValue)) {
            DisplayStar(true, rowIndex);
        } else {
            DisplayStar(false, rowIndex);
            lcd.setCursor(colIndex, rowIndex);
            lcd.print(F(NAN_TEXT));
        }
    }

    previousValue = value;
}

void LCDManager::DisplayStar(bool display, uint8_t rowIndex) {
    lcd.setCursor(STAR_COL_INDEX, rowIndex);
    lcd.print(display ? F(STAR_TEXT) : F(NO_STAR_TEXT));
}

void LCDManager::InitPreviousData(float const &temperature, float const &humidity) {
    if (!isnan(temperature)) {
        previousTemperature = temperature;
    }
    if (!isnan(humidity)) {
        previousHumidity = humidity;
    }
}

void LCDManager::DisplayMinMaxValues() {
    lcd.clear();
    lcd.home();
    lcd.print(F(MINMAX_TEMP_TEXT));
    lcd.write(DEGREE_CHAR_ID);
    lcd.setCursor(MINMAX_TEXT_SEP_COL, MINMAX_TEXT_START_ROW);
    lcd.print(F(MINMAX_SEP_TEXT));
    lcd.setCursor(MINMAX_TEXT_START_COL, MINMAX_TEXT_RH_ROW);
    lcd.print(F(MINMAX_RH_TEXT));
    lcd.setCursor(MINMAX_TEXT_SEP_COL, MINMAX_TEXT_RH_ROW);
    lcd.print(F(MINMAX_SEP_TEXT));

    DisplayMinMaxValue((*minMaxComp)->GetTempMin(), MIN_TEMP_COL_INDEX, MIN_TEMP_ROW_INDEX);
    DisplayMinMaxValue((*minMaxComp)->GetTempMax(), MAX_TEMP_COL_INDEX, MAX_TEMP_ROW_INDEX);
    DisplayMinMaxValue((*minMaxComp)->GetRHMin(), MIN_RH_COL_INDEX, MIN_RH_ROW_INDEX);
    DisplayMinMaxValue((*minMaxComp)->GetRHMax(), MAX_RH_COL_INDEX, MAX_RH_ROW_INDEX);
}

void LCDManager::DisplayInitTextAndTempRHValues() {
    lcd.clear();
//    delayMicroseconds(1000);
    DisplayInitText();
    ProperDisplay(previousTemperature, previousTemperature, TEMP_COL_INDEX, TEMP_LINE_INDEX, true);
    ProperDisplay(previousHumidity, previousHumidity, RH_COL_INDEX, RH_LINE_INDEX, true);
}

void LCDManager::DisplayMinMaxValue(float const value, int const colId, int const rowId)  {
    lcd.setCursor(colId, rowId);
    lcd.print(value, DIGIT_NUMBER);
}

