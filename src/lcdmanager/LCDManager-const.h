//
// Created by vincent on 13/03/2021.
//

#ifndef THERMO_LAB_LCDMANAGER_CONST_H
#define THERMO_LAB_LCDMANAGER_CONST_H

#define BOOT_TEXT           "Thermo Lab"
#define TEMP_DEFAULT_TEXT   "Temp --.-"
#define DEGREE_SIGN_TEXT    "C"
#define RH_DEFAULT_TEXT     " RH  --.-%"
#define NAN_TEXT            "--.-"
#define STAR_TEXT           "*"
#define NO_STAR_TEXT        " "

#define DEGREE_CHAR_ID     0
#define DIGIT_NUMBER       1
#define TEMP_LINE_INDEX    0
#define TEMP_COL_INDEX     5
#define RH_LINE_INDEX      1
#define RH_START_COL_INDEX 0
#define RH_COL_INDEX       5
#define STAR_COL_INDEX     15

const PROGMEM char c_degreeSign[8] = {
        B01110,
        B01010,
        B01110,
        B00000,
        B00000,
        B00000,
        B00000,
        B00000
};

#define MINMAX_TEMP_TEXT    "T"
#define MINMAX_TEMP_TEXT    "T"
#define MINMAX_RH_TEXT      "RH"
#define MINMAX_SEP_TEXT     "/"

#define MINMAX_TEXT_START_COL   0
#define MINMAX_TEXT_START_ROW   0
#define MINMAX_TEXT_RH_ROW      1
#define MINMAX_TEXT_SEP_COL     9

#define MIN_TEMP_COL_INDEX  4
#define MIN_TEMP_ROW_INDEX  0
#define MAX_TEMP_COL_INDEX  11
#define MAX_TEMP_ROW_INDEX  0
#define MIN_RH_COL_INDEX    4
#define MIN_RH_ROW_INDEX    1
#define MAX_RH_COL_INDEX    11
#define MAX_RH_ROW_INDEX    1

#endif //THERMO_LAB_LCDMANAGER_CONST_H
