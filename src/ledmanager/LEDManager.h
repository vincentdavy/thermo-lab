//
// Created by vincent on 30/01/2021.
//

#ifndef THERMO_LAB_LEDMANAGER_H
#define THERMO_LAB_LEDMANAGER_H

class LEDManager {
public:
    static void Init();

    static void SetLEDStatus(bool status);
};

#endif //THERMO_LAB_LEDMANAGER_H
