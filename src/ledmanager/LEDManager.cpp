//
// Created by vincent on 30/01/2021.
//

#include "LEDManager.h"
#include <Arduino.h>

void LEDManager::Init() {
    pinMode(LED_BUILTIN, OUTPUT);
}

void LEDManager::SetLEDStatus(bool status) {
    if (status) {
        digitalWrite(LED_BUILTIN, HIGH);
    } else {
        digitalWrite(LED_BUILTIN, LOW);
    }
}


