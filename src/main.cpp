#include <Arduino.h>


#ifdef DEBUG
#include "avr8-stub.h"
#endif

#include <sleep/sleep.h>
#include <core/core.h>

#include "ledmanager/LEDManager.h"

Core *core = nullptr;

void setup() {
#ifdef DEBUG
    debug_init();
    debug_message("Debbuger waiting");
    breakpoint();
#endif
    core = new Core();
    Sleep::InitSleep();
    core->Init();
}

void loop() {
    core->CheckInterrupt();
    LEDManager::SetLEDStatus(true);
    core->DisplayCurrentTempRH();
    LEDManager::SetLEDStatus(false);
    Sleep::NormalSleep();
}