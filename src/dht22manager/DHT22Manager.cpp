//
// Created by vincent on 30/01/2021.
//

#include "DHT22Manager.h"

void DHT22Manager::ReadValues(float & temperature, float & humidity) {
    int err = dht.read2(&temperature, &humidity, nullptr);
    if (err != SimpleDHTErrSuccess) {
        temperature = NAN;
        humidity = NAN;
    }
}
