//
// Created by vincent on 30/01/2021.
//

#ifndef THERMO_LAB_DHT22MANAGER_H
#define THERMO_LAB_DHT22MANAGER_H

#include <define.h>
#include <SimpleDHT.h>

/*!
 * @brief Class to manage DHT 22 with abstraction
 */
class DHT22Manager {
private:
    SimpleDHT22 dht = SimpleDHT22(DHT_PIN);

public:
    /*!
     * @brief Read the DHT22 values
     * @param temperature param to return the temperature
     * @param humidity  param to return the humidity
     */
    void ReadValues(float & temperature, float & humidity);
};


#endif //THERMO_LAB_DHT22MANAGER_H
